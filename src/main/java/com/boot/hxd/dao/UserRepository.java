package com.boot.hxd.dao;

import com.boot.hxd.dao.mapper.UserRowMapper;
import com.boot.hxd.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Houxudong
 **/
@Repository
public class UserRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional(readOnly = true)
    public List<User> getAll(){
        String sql = "select * from user";
        List<User> userList = jdbcTemplate.query(sql, new UserRowMapper());
        return userList;
    }

    @Transactional(readOnly = true)
    public User getUserById(Integer id){
        String sql = "select * from user where id=?";
        Object[] params = new Object[]{id};
        return jdbcTemplate.queryForObject(sql,params,new UserRowMapper());
    }

    public int addUser(User user){
        String sql = "insert into user(name,password,tel) values(?,?,?)";
        Object[] params = new Object[]{user.getName(),user.getPassword(),user.getTel()};
        return jdbcTemplate.update(sql,params);
    }
    public int updateUser(User user){
        String sql = "update user set name=?,password=?,tel=? where id=?";
        Object[] params = new Object[]{user.getName(),user.getPassword(),user.getTel(),user.getId()};
        return jdbcTemplate.update(sql,params);
    }
}
