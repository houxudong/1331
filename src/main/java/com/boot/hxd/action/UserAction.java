package com.boot.hxd.action;

import com.boot.hxd.dao.UserRepository;
import com.boot.hxd.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Houxudong
 **/

@RestController
public class UserAction {
    @Autowired
    private UserRepository userDao;

    @RequestMapping("/getAll")
    public List<User> getAll(){
        System.out.println("xxx");
        List<User> users = userDao.getAll();
        return users;
    }

    @RequestMapping("/addUser/{name}")
    public String addUser(@PathVariable String name){
        User user = new User();
        user.setName(name);
        userDao.addUser(user);
        return "add user success!";
    }

    @RequestMapping("/updateUser/{id}")
    public String updateUser(@PathVariable Integer id){
        User user = new User();
        user.setId(id);
        user.setName("马玉");
        userDao.updateUser(user);
        return "update user success!";
    }

}
